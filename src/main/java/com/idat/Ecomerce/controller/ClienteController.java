package com.idat.Ecomerce.controller;

import com.idat.Ecomerce.model.Cliente;
import com.idat.Ecomerce.services.IClienteService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin(origins = "http://localhost:4201",maxAge = 3600)

@RestController
@RequestMapping("/client")
public class ClienteController {
    
    @Autowired
    private IClienteService clientService;
    
    @GetMapping("/listclientes")
    public List<Cliente> listaClientes(){
        return clientService.findAllClientes();
    }
}
