package com.idat.Ecomerce.controller;

import com.idat.Ecomerce.model.Producto;
import com.idat.Ecomerce.model.Upload;
import com.idat.Ecomerce.services.IProductoService;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@CrossOrigin(origins = "http://localhost:4201",maxAge = 3600)
@RestController
@RequestMapping("/product")
public class ProductoController {
    
    @Autowired
    IProductoService productoService;
    
    @GetMapping("/listproducts")
    public List<Producto> listarProductos(){
        return productoService.findAllProductos();
    }
    
    @GetMapping("/nameproduct")
    public Producto getProducto(@RequestParam("name") String nombre_producto){
        return productoService.findByName(nombre_producto);
    }
    
    @GetMapping("/search")
    public List<Producto> buscarProducto(@RequestParam("q") String query){
        return productoService.findByNameLike("%"+query+"%");
    }
    
    @GetMapping("/category")
    public List<Producto> bucarPorCategoria(@RequestParam("id") String nombre_categoria){
        return productoService.findByCategory(nombre_categoria);
    }
    
    @PostMapping("/save")
    public void registrarProducto(@RequestBody Producto producto){
        productoService.saveProduct(producto);
    }
    
    @DeleteMapping("/delete")
    public void eliminarProducto(@RequestParam("id") int id){
        productoService.deleteProduct(id);
    }
    
    @PostMapping("/upload")
    public void upload(@RequestParam("imagen") MultipartFile uploadingFiles){
        try {
            System.out.println(uploadingFiles.getSize());
            File file = new File("C:\\Angular\\DonPepe\\src\\assets\\image\\"+uploadingFiles.getOriginalFilename());
            uploadingFiles.transferTo(file);
        } catch (IOException ex) {
            System.out.println(ex.getMessage()+" MENSAJE DE ERROR 1");
        } catch (IllegalStateException ex) {
            System.out.println(ex.getMessage()+" MENSAJE DE ERROR 2");
        }
    }
    
}
