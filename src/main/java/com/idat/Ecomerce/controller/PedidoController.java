package com.idat.Ecomerce.controller;

import com.idat.Ecomerce.model.Pedido_Producto;
import com.idat.Ecomerce.services.IPedidosService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation. RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin(origins = "http://localhost:4201",maxAge = 3600)
@RestController
@RequestMapping("/order")
public class PedidoController {
    
    @Autowired
    private IPedidosService pedidoService;
    
    @PostMapping("/save")
    public void registrarPedido(@RequestBody Pedido_Producto p){
        pedidoService.savePedido(p);
    }
    
    @GetMapping("/listorders")
    public List<Pedido_Producto> listaPedidos(){
        return pedidoService.findAll();
    }
    
    @DeleteMapping("/delete")
    public void eliminarPedido(@RequestParam("id") int id){
        pedidoService.deletePedido(id);
    }
    
    @GetMapping("/cantidad")
    public int getCantidadPedido(@RequestParam("id")int id){
        return pedidoService.quanity(id);
    }
}
