package com.idat.Ecomerce.services;

import com.idat.Ecomerce.model.Cliente;
import com.idat.Ecomerce.repository.Cliente_Repository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ClientesServiceImpl implements IClienteService{
    
    @Autowired
    private Cliente_Repository repoCliente;

    @Override
    public List<Cliente> findAllClientes() {
        return repoCliente.findAll();
    }
    
    
}
