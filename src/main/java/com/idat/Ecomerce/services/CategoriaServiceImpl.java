package com.idat.Ecomerce.services;

import com.idat.Ecomerce.model.Categoria;
import com.idat.Ecomerce.repository.CategoriaRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CategoriaServiceImpl implements ICategoriaServices{

    @Autowired
    private CategoriaRepository repo;
    
    @Override
    public List<Categoria> findAllCategorias() {
        return repo.findAll();
    }
    
}
