package com.idat.Ecomerce.services;

import com.idat.Ecomerce.model.Cliente;
import com.idat.Ecomerce.model.Pedido;
import com.idat.Ecomerce.model.Pedido_Producto;
import com.idat.Ecomerce.repository.PedidoRepository;
import com.idat.Ecomerce.repository.Pedido_ProductoRepository;
import com.idat.Ecomerce.repository.ProductoRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PedidoServiceImpl implements IPedidosService{

    @Autowired
    private PedidoRepository repoPedido;
    
    @Autowired
    private Pedido_ProductoRepository repoPedidoProducto;
    
    @Autowired
    private ProductoRepository repoProducto;
    
    @Override
    public void savePedido(Pedido_Producto p) {
        System.out.println(p.toString());
        if(p.getIdPedidoProducto()==0){
            Pedido pedido = p.getPedido();
            repoPedido.save(pedido);
            p.setPedido(pedido);
            repoPedidoProducto.save(p);
        }else{
            repoPedidoProducto.save(p);
        }
        p.getProducto().setCantidad(p.getProducto().getCantidad()-p.getCantidad());
        repoProducto.save(p.getProducto());
    }

    @Override
    public List<Pedido_Producto> findAll() {
        return repoPedidoProducto.findAll();
    }

    @Override
    public void deletePedido(int id) {
        Pedido_Producto p= repoPedidoProducto.findById(id).get();
        repoPedidoProducto.deleteById(id);
        repoPedido.deleteById(p.getPedido().getIdPedido());
        p.getProducto().setCantidad(p.getProducto().getCantidad()+p.getCantidad());
        repoProducto.save(p.getProducto());
    }

    @Override
    public int quanity(int id) {
        List<Pedido_Producto>lista= repoPedidoProducto.findByPedido_Cliente_idCliente(id);
        int cantidad=0;
        for(Pedido_Producto p: lista){
            cantidad+=p.getCantidad();
        }
        return cantidad;
    }
    
    
}
