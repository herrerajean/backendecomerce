package com.idat.Ecomerce.services;

import com.idat.Ecomerce.model.Cliente;
import java.util.List;

public interface IClienteService {
    
    List<Cliente> findAllClientes();
}
