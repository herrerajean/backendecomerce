package com.idat.Ecomerce.services;

import com.idat.Ecomerce.model.Producto;
import com.idat.Ecomerce.model.Upload;
import java.util.List;

public interface IProductoService {
    
    List<Producto> findAllProductos();
    
    Producto findByName(String producto);
    
    List<Producto> findByNameLike(String producto);
    
    List<Producto> findByCategory(String nombre_categoria);
    
    void saveProduct(Producto producto);
    
    void deleteProduct(int id_product);
    
    void saveUpload(Upload u);
}
