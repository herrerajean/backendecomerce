package com.idat.Ecomerce.services;

import com.idat.Ecomerce.model.Categoria;
import java.util.List;

public interface ICategoriaServices {
    
    List<Categoria> findAllCategorias();
    
}
