package com.idat.Ecomerce.services;

import com.idat.Ecomerce.model.Categoria;
import com.idat.Ecomerce.model.Producto;
import com.idat.Ecomerce.model.Upload;
import com.idat.Ecomerce.repository.CategoriaRepository;
import com.idat.Ecomerce.repository.ProductoRepository;
import com.idat.Ecomerce.repository.UploadRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ProductoServiceImpl implements IProductoService{

    @Autowired
    private ProductoRepository repo;
    
    @Autowired
    private CategoriaRepository repoCat;
    
    @Autowired
    private UploadRepository repoUpload;
    
    @Override
    public List<Producto> findAllProductos() {
        return repo.findAll();
    }

    @Override
    public Producto findByName(String nombre_producto) {
        return repo.findByNombreProducto(nombre_producto).get(0);
    }

    @Override
    public List<Producto> findByNameLike(String producto) {
        return repo.findByNombreProductoLike(producto);
    }

    @Override
    public List<Producto> findByCategory(String nombre_categoria) {
        Categoria categoria = repoCat.findByNombreCategoria(nombre_categoria);
        return repo.findByCategoria(categoria);
    }

    @Override
    public void saveProduct(Producto producto) {
        repo.save(producto);
    }

    @Override
    public void deleteProduct(int id_product) {
        repo.deleteById(id_product);
    }

    @Override
    public void saveUpload(Upload u) {
        repoUpload.save(u);
    }
    
    
}
