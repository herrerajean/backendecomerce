package com.idat.Ecomerce.services;

import com.idat.Ecomerce.model.Pedido_Producto;
import java.util.List;

public interface IPedidosService {
    
    List<Pedido_Producto> findAll();
    
    void savePedido (Pedido_Producto p);
    
    void deletePedido(int id);
    
    int quanity(int id);
}
