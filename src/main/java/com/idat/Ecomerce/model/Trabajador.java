package com.idat.Ecomerce.model;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


@Entity
@Table(name = "trabajador")
public class Trabajador {
    
    @Id
    @Column(name = "id_trabajador")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int idTrabajador;
    
    @Column(name = "nombres_trabajador")
    private String nombreTrabajador;
    
    @Column(name = "apellidos_trabajador")
    private String apellidoTrabajador;
    
    private char sexo;
    
    private String area;
    
    private char turno;
    
    private double salario;
    
    @Column(name = "fecha_inicio")
    private Date fechaInicio;
    
    @Column(name = "fecha_sese")
    private Date fechaSese;
    
    private String direccion;
    
    private String celular;
    
    private String email;
    
    @Column(name = "documento_id")
    private String documentoId;
    
    @Column(name = "estado_vendedor")
    private String estadoVendedor;
    
    @Column(name = "estado_civil")
    private char estadoCivil;
    
    @ManyToOne
    @JoinColumn(name = "id_distrito")
    private Distrito distrito;
    
    @ManyToOne
    @JoinColumn(name = "id_cargo")
    private Cargo cargo;

    public Trabajador(int idTrabajador, String nombreTrabajador, String apellidoTrabajador, char sexo, String area, char turno, double salario, Date fechaInicio, Date fechaSese, String direccion, String celular, String email, String documentoId, String estadoVendedor, char estadoCivil, Distrito distrito, Cargo cargo) {
        this.idTrabajador = idTrabajador;
        this.nombreTrabajador = nombreTrabajador;
        this.apellidoTrabajador = apellidoTrabajador;
        this.sexo = sexo;
        this.area = area;
        this.turno = turno;
        this.salario = salario;
        this.fechaInicio = fechaInicio;
        this.fechaSese = fechaSese;
        this.direccion = direccion;
        this.celular = celular;
        this.email = email;
        this.documentoId = documentoId;
        this.estadoVendedor = estadoVendedor;
        this.estadoCivil = estadoCivil;
        this.distrito = distrito;
        this.cargo = cargo;
    }

    public Trabajador() {
    }

    public int getIdTrabajador() {
        return idTrabajador;
    }

    public void setIdTrabajador(int idTrabajador) {
        this.idTrabajador = idTrabajador;
    }

    public String getNombreTrabajador() {
        return nombreTrabajador;
    }

    public void setNombreTrabajador(String nombreTrabajador) {
        this.nombreTrabajador = nombreTrabajador;
    }

    public String getApellidoTrabajador() {
        return apellidoTrabajador;
    }

    public void setApellidoTrabajador(String apellidoTrabajador) {
        this.apellidoTrabajador = apellidoTrabajador;
    }

    public char getSexo() {
        return sexo;
    }

    public void setSexo(char sexo) {
        this.sexo = sexo;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public char getTurno() {
        return turno;
    }

    public void setTurno(char turno) {
        this.turno = turno;
    }

    public double getSalario() {
        return salario;
    }

    public void setSalario(double salario) {
        this.salario = salario;
    }

    public Date getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(Date fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public Date getFechaSese() {
        return fechaSese;
    }

    public void setFechaSese(Date fechaSese) {
        this.fechaSese = fechaSese;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getCelular() {
        return celular;
    }

    public void setCelular(String celular) {
        this.celular = celular;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getDocumentoId() {
        return documentoId;
    }

    public void setDocumentoId(String documentoId) {
        this.documentoId = documentoId;
    }

    public String getEstadoVendedor() {
        return estadoVendedor;
    }

    public void setEstadoVendedor(String estadoVendedor) {
        this.estadoVendedor = estadoVendedor;
    }

    public char getEstadoCivil() {
        return estadoCivil;
    }

    public void setEstadoCivil(char estadoCivil) {
        this.estadoCivil = estadoCivil;
    }

    public Distrito getDistrito() {
        return distrito;
    }

    public void setDistrito(Distrito distrito) {
        this.distrito = distrito;
    }

    public Cargo getCargo() {
        return cargo;
    }

    public void setCargo(Cargo cargo) {
        this.cargo = cargo;
    }
    
    
}
