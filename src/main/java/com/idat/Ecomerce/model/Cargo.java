package com.idat.Ecomerce.model;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "cargo")
public class Cargo {
    
    @Id
    @Column(name = "id_cargo")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int idCargo;
    
    @Column(name = "nombre_cargo")
    private String nombreCargo;
    
    @Column(name = "fecha_creacion")
    private Date fechaCreacion;
    
    @Column(name = "estado_cargo")
    private String estadoCargo;

    public Cargo(int idCargo, String nombreCargo, Date fechaCreacion, String estadoCargo) {
        this.idCargo = idCargo;
        this.nombreCargo = nombreCargo;
        this.fechaCreacion = fechaCreacion;
        this.estadoCargo = estadoCargo;
    }

    public Cargo() {
    }

    public int getIdCargo() {
        return idCargo;
    }

    public void setIdCargo(int idCargo) {
        this.idCargo = idCargo;
    }

    public String getNombreCargo() {
        return nombreCargo;
    }

    public void setNombreCargo(String nombreCargo) {
        this.nombreCargo = nombreCargo;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public String getEstadoCargo() {
        return estadoCargo;
    }

    public void setEstadoCargo(String estadoCargo) {
        this.estadoCargo = estadoCargo;
    }
    
    
}
