package com.idat.Ecomerce.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "categoria")
public class Categoria {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id_categoria;
    
    @Column(name = "nombre_categoria")
    private String nombreCategoria;
    
    private String estado;
    

    public Categoria(int id_categoria, String nombre_categoria, String estado) {
        this.id_categoria = id_categoria;
        this.nombreCategoria = nombre_categoria;
        this.estado = estado;
    }

    public Categoria() {
    }

    public int getId_categoria() {
        return id_categoria;
    }

    public void setId_categoria(int id_categoria) {
        this.id_categoria = id_categoria;
    }

    public String getNombre_categoria() {
        return nombreCategoria;
    }

    public void setNombre_categoria(String nombre_categoria) {
        this.nombreCategoria = nombre_categoria;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    @Override
    public String toString() {
        return "Categoria{" + "id_categoria=" + id_categoria + ", nombreCategoria=" + nombreCategoria + ", estado=" + estado + '}';
    }
    
    
}
