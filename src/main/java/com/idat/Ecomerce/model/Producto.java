package com.idat.Ecomerce.model;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "producto")
public class Producto {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_producto")
    private int idProducto;
    
    @Column(name = "nombre_producto")
    private String nombreProducto;
    private int cantidad;
    private double precio;
    private String url;
    @Column(name = "fecha_registo")
    private Date fechaRegisto;
    private String estado;
    
    @ManyToOne
    @JoinColumn(name = "id_categoria")
    private Categoria categoria;

    public Producto() {
    }

    public Producto(int id_producto, String nombre_producto, int cantidad, String url,double precio, Date fecha_registro, String estado, Categoria categoria) {
        this.idProducto = id_producto;
        this.nombreProducto = nombre_producto;
        this.cantidad = cantidad;
        this.precio = precio;
        this.url=url;
        this.fechaRegisto = fecha_registro;
        this.estado = estado;
        this.categoria = categoria;
    }

    public int getId_producto() {
        return idProducto;
    }

    public void setId_producto(int id_producto) {
        this.idProducto = id_producto;
    }

    public String getNombre_producto() {
        return nombreProducto;
    }

    public void setNombre_producto(String nombre_producto) {
        this.nombreProducto = nombre_producto;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Date getFecha_registro() {
        return fechaRegisto;
    }

    public void setFecha_registro(Date fecha_registro) {
        this.fechaRegisto = fecha_registro;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public Categoria getCategoria() {
        return categoria;
    }

    public void setCategoria(Categoria categoria) {
        this.categoria = categoria;
    }

    @Override
    public String toString() {
        return "Producto{" + "idProducto=" + idProducto + ", nombreProducto=" + nombreProducto + ", cantidad=" + cantidad + ", precio=" + precio + ", fechaRegisto=" + fechaRegisto + ", estado=" + estado + ", categoria=" + categoria + '}';
    }
    
    
    
}
