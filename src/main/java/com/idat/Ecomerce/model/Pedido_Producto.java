package com.idat.Ecomerce.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


@Entity
@Table(name = "pedido_producto")
public class Pedido_Producto {
    
    @Id
    @Column(name = "id_pedido_producto")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int idPedidoProducto;
    
    private int cantidad;
    
    private String descripcion;
    
    @Column(name = "precio_unitario")
    private double precioUnitario;
    
    @Column(name = "precio_total")
    private double precioTotal;
    
    @ManyToOne
    @JoinColumn(name = "id_pedido")
    private Pedido pedido;
    
    @ManyToOne
    @JoinColumn(name = "id_producto")
    private Producto producto;
    
    
    public Pedido_Producto(int idPedidoProducto, int cantidad, String descripcion, double precioUnitario, double precioTotal, Pedido pedido, Producto producto) {
        this.idPedidoProducto = idPedidoProducto;
        this.cantidad = cantidad;
        this.descripcion = descripcion;
        this.precioUnitario = precioUnitario;
        this.precioTotal = precioTotal;
        this.pedido = pedido;
        this.producto = producto;
    }
    
    public Pedido_Producto() {
    }

    public int getIdPedidoProducto() {
        return idPedidoProducto;
    }
    
    public void setIdPedidoProducto(int idPedidoProducto) {
        this.idPedidoProducto = idPedidoProducto;
    }
    
    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public double getPrecioUnitario() {
        return precioUnitario;
    }

    public void setPrecioUnitario(double precioUnitario) {
        this.precioUnitario = precioUnitario;
    }

    public double getPrecioTotal() {
        return precioTotal;
    }

    public void setPrecioTotal(double precioTotal) {
        this.precioTotal = precioTotal;
    }

    public Pedido getPedido() {
        return pedido;
    }

    public void setPedido(Pedido pedido) {
        this.pedido = pedido;
    }

    public Producto getProducto() {
        return producto;
    }

    public void setProducto(Producto producto) {
        this.producto = producto;
    }

    @Override
    public String toString() {
        return "Pedido_Producto{" + "idPedidoProducto=" + idPedidoProducto + ", cantidad=" + cantidad + ", descripcion=" + descripcion + ", precioUnitario=" + precioUnitario + ", precioTotal=" + precioTotal + ", pedido=" + pedido + ", producto=" + producto + '}';
    }
    
    
}
