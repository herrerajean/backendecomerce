package com.idat.Ecomerce.repository;

import com.idat.Ecomerce.model.Pedido_Producto;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface Pedido_ProductoRepository extends JpaRepository<Pedido_Producto, Integer>{
    List<Pedido_Producto> findByPedido_Cliente_idCliente(int id);
}
