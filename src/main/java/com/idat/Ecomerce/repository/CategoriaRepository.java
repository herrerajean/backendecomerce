package com.idat.Ecomerce.repository;

import com.idat.Ecomerce.model.Categoria;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CategoriaRepository extends JpaRepository<Categoria, Integer>{
    
    Categoria findByNombreCategoria(String nombre_categoria);
}
