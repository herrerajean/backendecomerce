package com.idat.Ecomerce.repository;

import com.idat.Ecomerce.model.Categoria;
import com.idat.Ecomerce.model.Producto;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductoRepository extends JpaRepository<Producto, Integer>{
    
    List<Producto> findByNombreProducto(String nombre_producto);
    
    List<Producto> findByNombreProductoLike(String producto);
    
    List<Producto> findByCategoria(Categoria categoria);
}
