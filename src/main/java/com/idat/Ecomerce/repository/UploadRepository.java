package com.idat.Ecomerce.repository;

import com.idat.Ecomerce.model.Upload;
import java.sql.Blob;

import org.springframework.data.jpa.repository.JpaRepository;

public interface UploadRepository extends JpaRepository<Upload,Integer>{
    
}
