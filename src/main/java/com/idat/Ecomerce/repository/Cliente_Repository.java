package com.idat.Ecomerce.repository;

import com.idat.Ecomerce.model.Cliente;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface Cliente_Repository extends JpaRepository<Cliente, Integer>{
    
}
